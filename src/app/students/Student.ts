export class Student {
  id : number;
  name : string;
  address: string;
  phone: string;
  isActive: boolean;
  course: string;

}
