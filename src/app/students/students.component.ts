import { Component, OnInit } from '@angular/core';
import {Student} from "./Student";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.less']
})
export class StudentsComponent implements OnInit {
  public students: Student[] = []
public studentForChange: Student;
  private studentsNames : string[] = ['Tanel', 'Annika', 'Sergei','Jhon', 'Dmitrii']
 private addresses: string[] = ['Tartu', 'Tallin', 'Narva', 'Moscow', 'London'];
 private phones: string[] = ['54543532','4324324','432432432','53255435545','5325434432'];
 private courses: string[] = ['SDA', 'Geek', 'Big', 'Neek', 'Week'];

  constructor(private modalService: NgbModal) {}


  ngOnInit(): void {
    for (const [i,name] of this.studentsNames.entries()) {
      const student = new Student();
      student.address=this.addresses[Math.floor(Math.random()*this.addresses.length)]
      student.course=this.courses[Math.floor(Math.random()*this.courses.length)]
      student.phone=this.phones[Math.floor(Math.random()*this.phones.length)]
      student.id=i;
      student.isActive= Math.random()>0.5;
 student.name=name;
      this.students.push(student);
      this.studentForChange = this.students[0];
    }

  }


  open(content, id) {
    let newStudent;
    if (id==null) {
      this.studentForChange= new Student();
      this.studentForChange.id=this.students.map(student => student.id).sort().pop()+1;
      this.students.push(this.studentForChange);
      newStudent=true;
    }
    else this.studentForChange=this.students.filter(student => student.id===id)[0];
    let nonChanged = {...this.studentForChange};
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
       if (!result) this.students=this.students.filter(student => student.id!==id);
      this.students.sort((s1,s2) => s1.id-s2.id);
    }, (reason) => {
      this.students=this.students.filter(student => student.id!==this.studentForChange.id);
      this.studentForChange=nonChanged;
      if (!newStudent) this.students.push(this.studentForChange);
      this.students.sort((s1,s2) => s1.id-s2.id);
    });
  }

}
